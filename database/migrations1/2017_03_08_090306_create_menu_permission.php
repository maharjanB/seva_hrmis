<?php

use App\Modules\Foundation\Database\Migration\CustomBluePrint;
use App\Modules\Foundation\Database\Migration\CustomMigration;
class CreateMenuPermission extends CustomMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->schema->create('tbl_menu_permission', function (CustomBluePrint $table) {
            $table->increments('id_menu_permission');
            $table->Integer('menu_id');
            $table->Integer('role_id');
            $table->Integer('permission_id');
            $table->tinyInteger('status');
            $table->authors();
            $table->timestamps();
            $table->foreign('menu_id')->references('id_menu')->on('tbl_menu');
            $table->foreign('permission_id')->references('id_permission')->on('tbl_permission');
            $table->foreign('role_id')->references('id_user_group')->on('tbl_user_group');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->schema->dropIfExists('tbl_user_group');

    }
}
