<?php

use App\Modules\Foundation\Database\Migration\CustomBluePrint;
use App\Modules\Foundation\Database\Migration\CustomMigration;


class CreateUniversitiesTable extends CustomMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->schema->create('tbl_university', function (CustomBluePrint $table) {
            $table->increments('id_university');
            $table->string('title');
            $table->string('desc');
            $table->tinyInteger('status');
            $table->authors();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_university');
    }
}
