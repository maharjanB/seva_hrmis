<?php

use App\Modules\Foundation\Database\Migration\CustomBluePrint;
use App\Modules\Foundation\Database\Migration\CustomMigration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEducationTable extends CustomMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->schema->create('tbl_education', function (CustomBluePrint $table) {
            $table->increments('id_education');
            $table->string('title')->nullable();
            $table->string('description');
            $table->tinyInteger('status');
            $table->authors();
            $table->timestamps();

    });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->schema->dropIfExists('tbl_education');
    }
}
