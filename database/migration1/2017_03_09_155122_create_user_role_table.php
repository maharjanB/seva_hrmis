<?php

use App\Modules\Foundation\Database\Migration\CustomBluePrint;
use App\Modules\Foundation\Database\Migration\CustomMigration;


class CreateUserRoleTable extends CustomMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->schema->create('tbl_role', function (CustomBluePrint $table) {
            $table->increments('id_role');
            $table->string('title');
            $table->string('desc');
            $table->tinyInteger('status');
            $table->authors();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->schema->dropIfExists('tbl_role');

    }
}
