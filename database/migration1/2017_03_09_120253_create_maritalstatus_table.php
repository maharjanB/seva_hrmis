<?php

use App\Modules\Foundation\Database\Migration\CustomBluePrint;
use App\Modules\Foundation\Database\Migration\CustomMigration;


class CreateMaritalstatusTable extends CustomMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->schema->create('tbl_maritalstatus', function (CustomBluePrint $table) {
            $table->increments('id_maritalstatus');
            $table->string('title');
            $table->string('desc');
            $table->tinyInteger('status');
            $table->authors();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_maritalstatus');
    }
}
