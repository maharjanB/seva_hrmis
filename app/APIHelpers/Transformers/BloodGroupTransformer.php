<?php

namespace App\APIHelpers\Transformers;

class BloodGroupTransformer extends Transformer
{
    public function transform($bloodGroup, $permission)
    {
        $data = [
            'id_blood_group' => isset($bloodGroup['id_blood_group']) ? $bloodGroup['id_blood_group'] : null,
            'desc' => isset($bloodGroup['desc']) ? $bloodGroup['desc'] : null,
            'status' => isset($bloodGroup['status']) ? $bloodGroup['status'] : null,
            'created_by' => isset($bloodGroup['created_by']) ? $bloodGroup['created_by'] : null,
            'updated_by' => isset($bloodGroup['updated_by']) ? $bloodGroup['updated_by'] : null
        ];
        return $data;
    }
}
