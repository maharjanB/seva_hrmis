<?php

namespace App\APIHelpers\Transformers;

class EducationTransformer extends Transformer
{
    public function transform($education, $permission)
    {
        $data = [
            'id_education' => isset($education['id_education']) ? $education['id_education'] : null,
            'title' => isset($education['title']) ? $education['title'] : null,
            'desc' => isset($education['description']) ? $education['description'] : null,
            'status' => isset($education['status']) ? $education['status'] : null,
            'created_by' => isset($education['created_by']) ? $education['created_by'] : null,
            'updated_by' => isset($education['updated_by']) ? $education['updated_by'] : null
        ];
        return $data;
    }
}
