<?php

namespace App\APIHelpers\Transformers;

class PostTransformer extends Transformer
{
    public function transform($post, $permission)
    {
        $data = [
            'id_post' => isset($post['id_post']) ? $post['id_post'] : null,
            'title'=> isset($post['title'])? $post['title']:null,
            'desc' => isset($post['desc']) ? $post['desc'] : null,
            'status' => isset($post['status']) ? $post['status'] : null,
            'created_by' => isset($post['created_by']) ? $post['created_by'] : null,
            'updated_by' => isset($post['updated_by']) ? $post['updated_by'] : null
        ];
        return $data;
    }
}
