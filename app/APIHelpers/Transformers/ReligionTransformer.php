<?php

namespace App\APIHelpers\Transformers;

class ReligionTransformer extends Transformer
{
    public function transform($religion, $permission)
    {
        $data = [
            'id_religion' => isset($religion['id_religion']) ? $religion['id_religion'] : null,
            'desc' => isset($religion['desc']) ? $religion['desc'] : null,
            'status' => isset($religion['status']) ? $religion['status'] : null,
            'created_by' => isset($religion['created_by']) ? $religion['created_by'] : null,
            'updated_by' => isset($religion['updated_by']) ? $religion['updated_by'] : null
        ];
        return $data;
    }
}
