<?php

namespace App\APIHelpers\Transformers;

class RelationshipTransformer extends Transformer
{
    public function transform($relationship, $permission)
    {
        $data = [
            'id_relationship' => isset($relationship['id_relationship']) ? $relationship['id_relationship'] : null,
            'title' => isset($relationship['title']) ? $relationship['title'] : null,
            'desc' => isset($relationship['desc']) ? $relationship['desc'] : null,
            'status' => isset($relationship['status']) ? $relationship['status'] : null,
            'created_by' => isset($relationship['created_by']) ? $relationship['created_by'] : null,
            'updated_by' => isset($relationship['updated_by']) ? $relationship['updated_by'] : null
        ];
        return $data;
    }
}
