<?php

namespace App\APIHelpers\Transformers;

class MaritalstatusTransformer extends Transformer
{
    public function transform($maritalstatus, $permission)
    {
        $data = [
            'id_maritalstatus' => isset($maritalstatus['id_maritalstatus']) ? $maritalstatus['id_maritalstatus'] : null,
            'title' => isset($maritalstatus['title']) ? $maritalstatus['title'] : null,
            'desc' => isset($maritalstatus['desc']) ? $maritalstatus['desc'] : null,
            'status' => isset($maritalstatus['status']) ? $maritalstatus['status'] : null,
            'created_by' => isset($maritalstatus['created_by']) ? $maritalstatus['created_by'] : null,
            'updated_by' => isset($maritalstatus['updated_by']) ? $maritalstatus['updated_by'] : null
        ];
        return $data;
    }
}
