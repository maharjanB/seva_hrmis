<?php

namespace App\APIHelpers\Transformers;

class UniversityTransformer extends Transformer
{
    public function transform($university, $permission)
    {
        $data = [
            'id_university' => isset($university['id_university']) ? $university['id_university'] : null,
            'title' => isset($university['title']) ? $university['title'] : null,
            'desc' => isset($university['desc']) ? $university['desc'] : null,
            'status' => isset($university['status']) ? $university['status'] : null,
            'created_by' => isset($university['created_by']) ? $university['created_by'] : null,
            'updated_by' => isset($university['updated_by']) ? $university['updated_by'] : null
        ];
        return $data;
    }
}
