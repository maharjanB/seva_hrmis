<?php

namespace App\APIHelpers\Transformers;

class SkillTransformer extends Transformer
{
    public function transform($skill, $permission)
    {
        $data = [
            'id_skill' => isset($skill['id_skill']) ? $skill['id_skill'] : null,
            'title' => isset($skill['title']) ? $skill['title'] : null,
            'desc' => isset($skill['desc']) ? $skill['desc'] : null,
            'status' => isset($skill['status']) ? $skill['status'] : null,
            'created_by' => isset($skill['created_by']) ? $skill['created_by'] : null,
            'updated_by' => isset($skill['updated_by']) ? $skill['updated_by'] : null
        ];
        return $data;
    }
}
