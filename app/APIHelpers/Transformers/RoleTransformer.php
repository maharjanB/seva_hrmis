<?php

namespace App\APIHelpers\Transformers;

class RoleTransformer extends Transformer
{
    public function transform($role, $permission)
    {
        $data = [
            'id_role' => isset($role['id_role']) ? $role['id_role'] : null,
            'title'=> isset($role['title'])? $role['title']:null,
            'desc' => isset($role['desc']) ? $role['desc'] : null,
            'status' => isset($role['status']) ? $role['status'] : null,
            'created_by' => isset($role['created_by']) ? $role['created_by'] : null,
            'updated_by' => isset($role['updated_by']) ? $role['updated_by'] : null
        ];
        return $data;
    }
}
