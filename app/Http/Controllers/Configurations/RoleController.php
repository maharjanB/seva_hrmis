<?php

namespace App\Http\Controllers\Configurations;

use App\APIHelpers\Transformers\RoleTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use App\Modules\Configurations\Services\RoleService;
use App\Modules\Foundation\Controllers\AbstractController;


class RoleController extends AbstractController
{
    protected $service;
    protected $dataTransformer;
    protected $primary_key = 'id_role';

    public function __construct(
        RoleService $service,
        RoleTransformer $roleTransformer
    ) {
        $this->service = $service;
        $this->dataTransformer = $roleTransformer;
    }

}
