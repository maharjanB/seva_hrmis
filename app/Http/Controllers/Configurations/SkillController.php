<?php

namespace App\Http\Controllers\Configurations;

use App\APIHelpers\Transformers\SkillTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use App\Modules\Configurations\Services\SkillService;
use App\Modules\Foundation\Controllers\AbstractController;


class SkillController extends AbstractController
{
    protected $service;
    protected $dataTransformer;
    protected $primary_key = 'id_skill';

    public function __construct(
        SkillService $service,
        SkillTransformer $skillTransformer
    ) {
        $this->service = $service;
        $this->dataTransformer = $skillTransformer;
    }

}
