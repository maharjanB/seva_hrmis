<?php

namespace App\Http\Controllers\Configurations;

use App\APIHelpers\Transformers\MaritalstatusTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use App\Modules\Configurations\Services\MaritalstatusService;
use App\Modules\Foundation\Controllers\AbstractController;


class MaritalstatusController extends AbstractController
{
    protected $service;
    protected $dataTransformer;
    protected $primary_key = 'id_maritalstatus';

    public function __construct(
        MaritalstatusService $service,
        MaritalstatusTransformer $maritalstatusTransformer
    ) {
        $this->service = $service;
        $this->dataTransformer = $maritalstatusTransformer;
    }

}
