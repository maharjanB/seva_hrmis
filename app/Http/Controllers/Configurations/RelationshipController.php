<?php

namespace App\Http\Controllers\Configurations;

use App\APIHelpers\Transformers\RelationshipTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use App\Modules\Configurations\Services\RelationshipService;
use App\Modules\Foundation\Controllers\AbstractController;


class RelationshipController extends AbstractController
{
    protected $service;
    protected $dataTransformer;
    protected $primary_key = 'id_relationship';

    public function __construct(
        RelationshipService $service,
        RelationshipTransformer $relationshipTransformer
    ) {
        $this->service = $service;
        $this->dataTransformer = $relationshipTransformer;
    }

}
