<?php

namespace App\Http\Controllers\Configurations;

use App\APIHelpers\Transformers\ReligionTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use App\Modules\Configurations\Services\ReligionService;
use App\Modules\Foundation\Controllers\AbstractController;


class ReligionController extends AbstractController
{
    protected $service;
    protected $dataTransformer;
    protected $primary_key = 'id_religion';

    public function __construct(
        ReligionService $service,
        ReligionTransformer $religionTransformer
    ) {
        $this->service = $service;
        $this->dataTransformer = $religionTransformer;
    }

}
