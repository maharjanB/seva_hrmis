<?php

namespace App\Modules\Configurations\Http\Controllers;

use App\APIHelpers\Transformers\BloodGroupTransformer;
use App\Modules\Configurations\Entities\BloodGroup;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use App\Modules\Configurations\Services\BloodGroupService;
use App\Modules\Foundation\Controllers\AbstractController;


class BloodGroupController extends AbstractController
{
    protected $service;
    protected $dataTransformer;
    protected $primary_key = 'id_blood_group';

    public function __construct(
        BloodGroupService $service,
        BloodGroupTransformer $bloodGroupTransformer
    ) {
        $this->service = $service;
        $this->dataTransformer = $bloodGroupTransformer;
    }

}
