<?php

namespace App\Modules\Configurations\Http\Controllers;

use App\APIHelpers\Transformers\EducationTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use App\Modules\Configurations\Services\EducationService;
use App\Modules\Foundation\Controllers\AbstractController;


class EducationController extends AbstractController
{
    protected $service;
    protected $dataTransformer;
    protected $primary_key = 'id_education';

    public function __construct(
        EducationService $service,
        EducationTransformer $educationTransformer
    ) {
        $this->service = $service;
        $this->dataTransformer = $educationTransformer;
    }

}
