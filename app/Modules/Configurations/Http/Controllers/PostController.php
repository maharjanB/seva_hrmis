<?php

namespace App\Modules\Configurations\Http\Controllers;

use App\APIHelpers\Transformers\PostTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use App\Modules\Configurations\Services\PostService;
use App\Modules\Foundation\Controllers\AbstractController;


class PostController extends AbstractController
{
    protected $service;
    protected $dataTransformer;
    protected $primary_key = 'id_post';

    public function __construct(
        PostService $service,
        PostTransformer $postTransformer
    ) {
        $this->service = $service;
        $this->dataTransformer = $postTransformer;
    }

}
