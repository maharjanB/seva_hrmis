<?php

namespace App\Modules\Configurations\Http\Controllers;

use App\APIHelpers\Transformers\UniversityTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use App\Modules\Configurations\Services\UniversityService;
use App\Modules\Foundation\Controllers\AbstractController;


class UniversityController extends AbstractController
{
    protected $service;
    protected $dataTransformer;
    protected $primary_key = 'id_university';

    public function __construct(
        UniversityService $service,
        UniversityTransformer $universityTransformer
    ) {
        $this->service = $service;
        $this->dataTransformer = $universityTransformer;
    }

}
