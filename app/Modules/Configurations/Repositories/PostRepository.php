<?php
namespace App\Modules\Configurations\Repositories;

use App\Modules\Configurations\Entities\Post;
use App\Modules\Configurations\Entities\PostInterface;
use App\Modules\Foundation\Repositories\AbstractRepository;

class PostRepository extends AbstractRepository implements PostRepositoryInterface
{
    protected $model;

    public function __construct(PostInterface $model)
    {
        $this->model = $model;
    }


}
