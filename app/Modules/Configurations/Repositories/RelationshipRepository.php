<?php
namespace App\Modules\Configurations\Repositories;

use App\Modules\Configurations\Entities\Relationship;
use App\Modules\Configurations\Entities\RelationshipInterface;
use App\Modules\Foundation\Repositories\AbstractRepository;

class RelationshipRepository extends AbstractRepository implements RelationshipRepositoryInterface
{
    protected $model;

    public function __construct(RelationshipInterface $model)
    {
        $this->model = $model;
    }


}
