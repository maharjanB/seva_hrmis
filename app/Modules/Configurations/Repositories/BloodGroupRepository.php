<?php
namespace App\Modules\Configurations\Repositories;

use App\Modules\Configurations\Entities\BloodGroup;
use App\Modules\Configurations\Entities\BloodGroupInterface;
use App\Modules\Foundation\Repositories\AbstractRepository;

class BloodGroupRepository extends AbstractRepository implements BloodGroupRepositoryInterface
{
    protected $model;

    public function __construct(BloodGroupInterface $model)
    {
        $this->model = $model;
    }


}
