<?php
namespace App\Modules\Configurations\Repositories;

use App\Modules\Configurations\Entities\Education;
use App\Modules\Configurations\Entities\EducationInterface;
use App\Modules\Foundation\Repositories\AbstractRepository;

class EducationRepository extends AbstractRepository implements EducationRepositoryInterface
{
    protected $model;

    public function __construct(EducationInterface $model)
    {
        $this->model = $model;
    }


}
