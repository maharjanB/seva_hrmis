<?php
namespace App\Modules\Configurations\Repositories;

use App\Modules\Configurations\Entities\Religion;
use App\Modules\Configurations\Entities\ReligionInterface;
use App\Modules\Foundation\Repositories\AbstractRepository;

class ReligionRepository extends AbstractRepository implements ReligionRepositoryInterface
{
    protected $model;

    public function __construct(ReligionInterface $model)
    {
        $this->model = $model;
    }


}
