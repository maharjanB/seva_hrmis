<?php
namespace App\Modules\Configurations\Repositories;

use App\Modules\Configurations\Entities\Maritalstatus;
use App\Modules\Configurations\Entities\MaritalstatusInterface;
use App\Modules\Foundation\Repositories\AbstractRepository;

class MaritalstatusRepository extends AbstractRepository implements MaritalstatusRepositoryInterface
{
    protected $model;

    public function __construct(MaritalstatusInterface $model)
    {
        $this->model = $model;
    }


}
