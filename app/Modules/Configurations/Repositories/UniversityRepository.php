<?php
namespace App\Modules\Configurations\Repositories;

use App\Modules\Configurations\Entities\University;
use App\Modules\Configurations\Entities\UniversityInterface;
use App\Modules\Foundation\Repositories\AbstractRepository;

class UniversityRepository extends AbstractRepository implements UniversityRepositoryInterface
{
    protected $model;

    public function __construct(UniversityInterface $model)
    {
        $this->model = $model;
    }


}
