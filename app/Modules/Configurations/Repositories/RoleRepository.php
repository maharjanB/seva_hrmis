<?php
namespace App\Modules\Configurations\Repositories;

use App\Modules\Configurations\Entities\Role;
use App\Modules\Configurations\Entities\RoleInterface;
use App\Modules\Foundation\Repositories\AbstractRepository;

class RoleRepository extends AbstractRepository implements RoleRepositoryInterface
{
    protected $model;

    public function __construct(RoleInterface $model)
    {
        $this->model = $model;
    }


}
