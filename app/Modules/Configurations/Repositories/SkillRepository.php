<?php
namespace App\Modules\Configurations\Repositories;

use App\Modules\Configurations\Entities\Skill;
use App\Modules\Configurations\Entities\SkillInterface;
use App\Modules\Foundation\Repositories\AbstractRepository;

class SkillRepository extends AbstractRepository implements SkillRepositoryInterface
{
    protected $model;

    public function __construct(SkillInterface $model)
    {
        $this->model = $model;
    }


}
