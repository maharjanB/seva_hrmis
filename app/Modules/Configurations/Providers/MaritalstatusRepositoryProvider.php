<?php


namespace App\Modules\Configurations\Providers;

use App\Modules\Configurations\Entities\MaritalstatusInterface;
use Illuminate\Support\ServiceProvider;
use App\Modules\Configurations\Repositories\MaritalstatusRepository;
use App\Modules\Configurations\Repositories\MaritalstatusRepositoryInterface;

class MaritalstatusRepositoryProvider extends ServiceProvider
{
    /**
     * Register Service into the Container.
     *
     * @return MaritalstatusRepository
     */
    public function register()
    {
        // Register MaritalstatusRepositoryInterface
        $this->container = $this->app;
        $this->container->bind(MaritalstatusRepositoryInterface::class, function () {
            return new MaritalstatusRepository($this->app[MaritalstatusInterface::class]);
        });
    }
}
