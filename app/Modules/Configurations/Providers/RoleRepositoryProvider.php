<?php


namespace App\Modules\Configurations\Providers;

use App\Modules\Configurations\Entities\RoleInterface;
use Illuminate\Support\ServiceProvider;
use App\Modules\Configurations\Repositories\RoleRepository;
use App\Modules\Configurations\Repositories\RoleRepositoryInterface;

class RoleRepositoryProvider extends ServiceProvider
{
    /**
     * Register Service into the Container.
     *
     * @return RoleRepository
     */
    public function register()
    {
        // Register RoleRepositoryInterface
        $this->container = $this->app;
        $this->container->bind(RoleRepositoryInterface::class, function () {
            return new RoleRepository($this->app[RoleInterface::class]);
        });
    }
}
