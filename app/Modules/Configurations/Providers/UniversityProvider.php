<?php

namespace App\Modules\Configurations\Providers;

use Illuminate\Support\ServiceProvider;
use App\Modules\Configurations\Entities\University;
use App\Modules\Configurations\Entities\UniversityInterface;

class UniversityProvider extends ServiceProvider
{
    /**
     * Register Service into the Container.
     *
     * @return UniversityModel
     */
    public function register()
    {
        //Register BuModelInterface
        $this->container = $this->app;
        $this->container->bind(UniversityInterface::class, function () {
            return new University();
        });
    }
}
