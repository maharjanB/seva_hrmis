<?php


namespace App\Modules\Configurations\Providers;

use App\Modules\Configurations\Entities\UniversityInterface;
use Illuminate\Support\ServiceProvider;
use App\Modules\Configurations\Repositories\UniversityRepository;
use App\Modules\Configurations\Repositories\UniversityRepositoryInterface;

class UniversityRepositoryProvider extends ServiceProvider
{
    /**
     * Register Service into the Container.
     *
     * @return UniversityRepository
     */
    public function register()
    {
        // Register UniversityRepositoryInterface
        $this->container = $this->app;
        $this->container->bind(UniversityRepositoryInterface::class, function () {
            return new UniversityRepository($this->app[UniversityInterface::class]);
        });
    }
}
