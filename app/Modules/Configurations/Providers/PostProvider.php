<?php

namespace App\Modules\Configurations\Providers;

use Illuminate\Support\ServiceProvider;
use App\Modules\Configurations\Entities\Post;
use App\Modules\Configurations\Entities\PostInterface;

class PostProvider extends ServiceProvider
{
    /**
     * Register Service into the Container.
     *
     * @return PostModel
     */
    public function register()
    {
        //Register BuModelInterface
        $this->container = $this->app;
        $this->container->bind(PostInterface::class, function () {
            return new Post();
        });
    }
}
