<?php


namespace App\Modules\Configurations\Providers;

use App\Modules\Configurations\Entities\ReligionInterface;
use Illuminate\Support\ServiceProvider;
use App\Modules\Configurations\Repositories\ReligionRepository;
use App\Modules\Configurations\Repositories\ReligionRepositoryInterface;

class ReligionRepositoryProvider extends ServiceProvider
{
    /**
     * Register Service into the Container.
     *
     * @return ReligionRepository
     */
    public function register()
    {
        // Register ReligionRepositoryInterface
        $this->container = $this->app;
        $this->container->bind(ReligionRepositoryInterface::class, function () {
            return new ReligionRepository($this->app[ReligionInterface::class]);
        });
    }
}
