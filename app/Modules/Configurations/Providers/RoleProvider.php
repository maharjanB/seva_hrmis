<?php

namespace App\Modules\Configurations\Providers;

use Illuminate\Support\ServiceProvider;
use App\Modules\Configurations\Entities\Role;
use App\Modules\Configurations\Entities\RoleInterface;

class RoleProvider extends ServiceProvider
{
    /**
     * Register Service into the Container.
     *
     * @return RoleModel
     */
    public function register()
    {
        //Register BuModelInterface
        $this->container = $this->app;
        $this->container->bind(RoleInterface::class, function () {
            return new Role();
        });
    }
}
