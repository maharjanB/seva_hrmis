<?php

namespace App\Modules\Configurations\Providers;

use Illuminate\Support\ServiceProvider;
use App\Modules\Configurations\Entities\Education;
use App\Modules\Configurations\Entities\EducationInterface;

class EducationProvider extends ServiceProvider
{
    /**
     * Register Service into the Container.
     *
     * @return EducationModel
     */
    public function register()
    {
        //Register BuModelInterface
        $this->container = $this->app;
        $this->container->bind(EducationInterface::class, function () {
            return new Education();
        });
    }
}
