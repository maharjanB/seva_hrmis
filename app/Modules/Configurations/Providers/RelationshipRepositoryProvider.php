<?php


namespace App\Modules\Configurations\Providers;

use App\Modules\Configurations\Entities\RelationshipInterface;
use Illuminate\Support\ServiceProvider;
use App\Modules\Configurations\Repositories\RelationshipRepository;
use App\Modules\Configurations\Repositories\RelationshipRepositoryInterface;

class RelationshipRepositoryProvider extends ServiceProvider
{
    /**
     * Register Service into the Container.
     *
     * @return RelationshipRepository
     */
    public function register()
    {
        // Register RelationshipRepositoryInterface
        $this->container = $this->app;
        $this->container->bind(RelationshipRepositoryInterface::class, function () {
            return new RelationshipRepository($this->app[RelationshipInterface::class]);
        });
    }
}
