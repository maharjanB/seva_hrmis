<?php

namespace App\Modules\Configurations\Providers;

use Illuminate\Support\ServiceProvider;
use App\Modules\Configurations\Entities\Maritalstatus;
use App\Modules\Configurations\Entities\MaritalstatusInterface;

class MaritalstatusProvider extends ServiceProvider
{
    /**
     * Register Service into the Container.
     *
     * @return MaritalstatusModel
     */
    public function register()
    {
        //Register BuModelInterface
        $this->container = $this->app;
        $this->container->bind(MaritalstatusInterface::class, function () {
            return new Maritalstatus();
        });
    }
}
