<?php

namespace App\Modules\Configurations\Providers;

use Illuminate\Support\ServiceProvider;
use App\Modules\Configurations\Entities\Religion;
use App\Modules\Configurations\Entities\ReligionInterface;

class ReligionProvider extends ServiceProvider
{
    /**
     * Register Service into the Container.
     *
     * @return ReligionModel
     */
    public function register()
    {
        //Register BuModelInterface
        $this->container = $this->app;
        $this->container->bind(ReligionInterface::class, function () {
            return new Religion();
        });
    }
}
