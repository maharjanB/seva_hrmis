<?php

namespace App\Modules\Configurations\Providers;

use Illuminate\Support\ServiceProvider;
use App\Modules\Configurations\Entities\Skill;
use App\Modules\Configurations\Entities\SkillInterface;

class SkillProvider extends ServiceProvider
{
    /**
     * Register Service into the Container.
     *
     * @return SkillModel
     */
    public function register()
    {
        //Register BuModelInterface
        $this->container = $this->app;
        $this->container->bind(SkillInterface::class, function () {
            return new Skill();
        });
    }
}
