<?php


namespace App\Modules\Configurations\Providers;

use App\Modules\Configurations\Entities\PostInterface;
use Illuminate\Support\ServiceProvider;
use App\Modules\Configurations\Repositories\PostRepository;
use App\Modules\Configurations\Repositories\PostRepositoryInterface;

class PostRepositoryProvider extends ServiceProvider
{
    /**
     * Register Service into the Container.
     *
     * @return PostRepository
     */
    public function register()
    {
        // Register PostRepositoryInterface
        $this->container = $this->app;
        $this->container->bind(PostRepositoryInterface::class, function () {
            return new PostRepository($this->app[PostInterface::class]);
        });
    }
}
