<?php


namespace App\Modules\Configurations\Providers;

use App\Modules\Configurations\Entities\EducationInterface;
use Illuminate\Support\ServiceProvider;
use App\Modules\Configurations\Repositories\EducationRepository;
use App\Modules\Configurations\Repositories\EducationRepositoryInterface;

class EducationRepositoryProvider extends ServiceProvider
{
    /**
     * Register Service into the Container.
     *
     * @return EducationRepository
     */
    public function register()
    {
        // Register EducationRepositoryInterface
        $this->container = $this->app;
        $this->container->bind(EducationRepositoryInterface::class, function () {
            return new EducationRepository($this->app[EducationInterface::class]);
        });
    }
}
