<?php


namespace App\Modules\Configurations\Providers;

use App\Modules\Configurations\Entities\SkillInterface;
use Illuminate\Support\ServiceProvider;
use App\Modules\Configurations\Repositories\SkillRepository;
use App\Modules\Configurations\Repositories\SkillRepositoryInterface;

class SkillRepositoryProvider extends ServiceProvider
{
    /**
     * Register Service into the Container.
     *
     * @return SkillRepository
     */
    public function register()
    {
        // Register SkillRepositoryInterface
        $this->container = $this->app;
        $this->container->bind(SkillRepositoryInterface::class, function () {
            return new SkillRepository($this->app[SkillInterface::class]);
        });
    }
}
