<?php

namespace App\Modules\Configurations\Providers;

use Illuminate\Support\ServiceProvider;
use App\Modules\Configurations\Entities\BloodGroup;
use App\Modules\Configurations\Entities\BloodGroupInterface;

class BloodGroupProvider extends ServiceProvider
{
    /**
     * Register Service into the Container.
     *
     * @return BloodGroupModel
     */
    public function register()
    {
        //Register BuModelInterface
        $this->container = $this->app;
        $this->container->bind(BloodGroupInterface::class, function () {
            return new BloodGroup();
        });
    }
}
