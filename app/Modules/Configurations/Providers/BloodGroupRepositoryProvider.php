<?php


namespace App\Modules\Configurations\Providers;

use App\Modules\Configurations\Entities\BloodGroup;
use App\Modules\Configurations\Entities\BloodGroupInterface;
use Illuminate\Support\ServiceProvider;
use App\Modules\Configurations\Repositories\BloodGroupRepository;
use App\Modules\Configurations\Repositories\BloodGroupRepositoryInterface;

class BloodGroupRepositoryProvider extends ServiceProvider
{
    /**
     * Register Service into the Container.
     *
     * @return BloodGroupRepository
     */
    public function register()
    {
        // Register BloodGroupRepositoryInterface
        $this->container = $this->app;
        $this->container->bind(BloodGroupRepositoryInterface::class, function () {
            return new BloodGroupRepository($this->app[BloodGroupInterface::class]);
        });
    }
}
