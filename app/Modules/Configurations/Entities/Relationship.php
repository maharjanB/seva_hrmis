<?php

namespace App\Modules\Configurations\Entities;

use App\Modules\Foundation\Entities\BaseModel;
class Relationship extends BaseModel implements RelationshipInterface
{


    protected $table = 'tbl_relationship';

    protected $guarded = [];
    protected $primaryKey = 'id_relationship';

    protected $fillable = ['id_relationship', 'title','desc', 'status', 'created_by, updated_by', 'created_at', 'updated_at'];
}
