<?php

namespace App\Modules\Configurations\Entities;

use App\Modules\Foundation\Entities\BaseModel;
class BloodGroup extends BaseModel implements BloodGroupInterface
{


    protected $table = 'tbl_blood_group';

    protected $guarded = [];
    protected $primaryKey = 'id_blood_group';

    protected $fillable = ['id_blood_group', 'desc', 'status', 'created_by, updated_by', 'created_at', 'updated_at'];
}
