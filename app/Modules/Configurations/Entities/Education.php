<?php

namespace App\Modules\Configurations\Entities;

use App\Modules\Foundation\Entities\BaseModel;
class Education extends BaseModel implements EducationInterface
{


    protected $table = 'tbl_education';

    protected $guarded = [];
    protected $primaryKey = 'id_education';

    protected $fillable = ['id_education', 'title','description', 'status', 'created_by, updated_by', 'created_at', 'updated_at'];
}
