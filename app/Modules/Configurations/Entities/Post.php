<?php

namespace App\Modules\Configurations\Entities;

use App\Modules\Foundation\Entities\BaseModel;
class Post extends BaseModel implements PostInterface
{


    protected $table = 'tbl_post';

    protected $guarded = [];
    protected $primaryKey = 'id_post';

    protected $fillable = ['id_post','title', 'desc', 'status', 'created_by, updated_by', 'created_at', 'updated_at'];
}
