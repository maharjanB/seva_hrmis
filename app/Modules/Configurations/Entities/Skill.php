<?php

namespace App\Modules\Configurations\Entities;

use App\Modules\Foundation\Entities\BaseModel;
class Skill extends BaseModel implements SkillInterface
{


    protected $table = 'tbl_skill';

    protected $guarded = [];
    protected $primaryKey = 'id_skill';

    protected $fillable = ['id_skill', 'title','desc', 'status', 'created_by, updated_by', 'created_at', 'updated_at'];
}
