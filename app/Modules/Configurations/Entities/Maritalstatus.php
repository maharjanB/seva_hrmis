<?php

namespace App\Modules\Configurations\Entities;

use App\Modules\Foundation\Entities\BaseModel;
class Maritalstatus extends BaseModel implements MaritalstatusInterface
{


    protected $table = 'tbl_maritalstatus';

    protected $guarded = [];
    protected $primaryKey = 'id_maritalstatus';

    protected $fillable = ['id_maritalstatus', 'title','desc', 'status', 'created_by, updated_by', 'created_at', 'updated_at'];
}
