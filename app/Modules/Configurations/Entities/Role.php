<?php

namespace App\Modules\Configurations\Entities;

use App\Modules\Foundation\Entities\BaseModel;
class Role extends BaseModel implements RoleInterface
{


    protected $table = 'tbl_role';

    protected $guarded = [];
    protected $primaryKey = 'id_role';

    protected $fillable = ['id_role','title', 'desc', 'status', 'created_by, updated_by', 'created_at', 'updated_at'];
}
