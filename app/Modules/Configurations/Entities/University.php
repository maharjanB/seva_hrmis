<?php

namespace App\Modules\Configurations\Entities;

use App\Modules\Foundation\Entities\BaseModel;
class University extends BaseModel implements UniversityInterface
{


    protected $table = 'tbl_university';

    protected $guarded = [];
    protected $primaryKey = 'id_university';

    protected $fillable = ['id_university', 'title','desc', 'status', 'created_by, updated_by', 'created_at', 'updated_at'];
}
