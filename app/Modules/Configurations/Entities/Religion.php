<?php

namespace App\Modules\Configurations\Entities;

use App\Modules\Foundation\Entities\BaseModel;
class Religion extends BaseModel implements ReligionInterface
{


    protected $table = 'tbl_religion';

    protected $guarded = [];
    protected $primaryKey = 'id_religion';

    protected $fillable = ['id_religion', 'desc', 'status', 'created_by, updated_by', 'created_at', 'updated_at'];
}
