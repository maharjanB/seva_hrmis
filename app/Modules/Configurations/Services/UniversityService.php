<?php

namespace App\Modules\Configurations\Services;

use App\Modules\Configurations\Repositories\UniversityRepositoryInterface;
use App\Modules\Configurations\Validators\UniversityValidator;
use App\Modules\Foundation\Services\AbstractService;

class UniversityService extends AbstractService
{
    protected $repository;
    protected $validator;

    public function __construct(UniversityRepositoryInterface $repository)
    {
        $this->repository = $repository;
        $this->validator = new UniversityValidator();
    }

}
