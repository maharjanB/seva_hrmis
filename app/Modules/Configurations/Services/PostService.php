<?php

namespace App\Modules\Configurations\Services;

use App\Modules\Configurations\Repositories\PostRepositoryInterface;
use App\Modules\Configurations\Validators\PostValidator;
use App\Modules\Foundation\Services\AbstractService;

class PostService extends AbstractService
{
    protected $repository;
    protected $validator;

    public function __construct(PostRepositoryInterface $repository)
    {
        $this->repository = $repository;
        $this->validator = new PostValidator();
    }

}
