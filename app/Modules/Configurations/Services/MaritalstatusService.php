<?php

namespace App\Modules\Configurations\Services;

use App\Modules\Configurations\Repositories\MaritalstatusRepositoryInterface;
use App\Modules\Configurations\Validators\MaritalstatusValidator;
use App\Modules\Foundation\Services\AbstractService;

class MaritalstatusService extends AbstractService
{
    protected $repository;
    protected $validator;

    public function __construct(MaritalstatusRepositoryInterface $repository)
    {
        $this->repository = $repository;
        $this->validator = new MaritalstatusValidator();
    }

}
