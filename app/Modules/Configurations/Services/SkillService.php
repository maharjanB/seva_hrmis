<?php

namespace App\Modules\Configurations\Services;

use App\Modules\Configurations\Repositories\SkillRepositoryInterface;
use App\Modules\Configurations\Validators\SkillValidator;
use App\Modules\Foundation\Services\AbstractService;

class SkillService extends AbstractService
{
    protected $repository;
    protected $validator;

    public function __construct(SkillRepositoryInterface $repository)
    {
        $this->repository = $repository;
        $this->validator = new SkillValidator();
    }

}
