<?php

namespace App\Modules\Configurations\Services;

use App\Modules\Configurations\Repositories\RelationshipRepositoryInterface;
use App\Modules\Configurations\Validators\RelationshipValidator;
use App\Modules\Foundation\Services\AbstractService;

class RelationshipService extends AbstractService
{
    protected $repository;
    protected $validator;

    public function __construct(RelationshipRepositoryInterface $repository)
    {
        $this->repository = $repository;
        $this->validator = new RelationshipValidator();
    }

}
