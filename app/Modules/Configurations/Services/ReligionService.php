<?php

namespace App\Modules\Configurations\Services;

use App\Modules\Configurations\Repositories\ReligionRepositoryInterface;
use App\Modules\Configurations\Validators\ReligionValidator;
use App\Modules\Foundation\Services\AbstractService;

class ReligionService extends AbstractService
{
    protected $repository;
    protected $validator;

    public function __construct(ReligionRepositoryInterface $repository)
    {
        $this->repository = $repository;
        $this->validator = new ReligionValidator();
    }

}
