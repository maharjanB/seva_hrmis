<?php

namespace App\Modules\Configurations\Services;

use App\Modules\Configurations\Repositories\BloodGroupRepositoryInterface;
use App\Modules\Configurations\Validators\BloodGroupValidator;
use App\Modules\Foundation\Services\AbstractService;

class BloodGroupService extends AbstractService
{
    protected $repository;
    protected $validator;

    public function __construct(BloodGroupRepositoryInterface $repository)
    {
        $this->repository = $repository;
        $this->validator = new BloodGroupValidator();
    }

}
