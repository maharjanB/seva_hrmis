<?php

namespace App\Modules\Configurations\Services;

use App\Modules\Configurations\Repositories\RoleRepositoryInterface;
use App\Modules\Configurations\Validators\RoleValidator;
use App\Modules\Foundation\Services\AbstractService;

class RoleService extends AbstractService
{
    protected $repository;
    protected $validator;

    public function __construct(RoleRepositoryInterface $repository)
    {
        $this->repository = $repository;
        $this->validator = new RoleValidator();
    }

}
