<?php

namespace App\Modules\Configurations\Services;

use App\Modules\Configurations\Repositories\EducationRepositoryInterface;
use App\Modules\Configurations\Validators\EducationValidator;
use App\Modules\Foundation\Services\AbstractService;

class EducationService extends AbstractService
{
    protected $repository;
    protected $validator;

    public function __construct(EducationRepositoryInterface $repository)
    {
        $this->repository = $repository;
        $this->validator = new EducationValidator();
    }

}
