<?php

namespace App\Modules\Configurations\Validators;

use App\Modules\Foundation\Validation\LaravelValidator;
use App\Modules\Foundation\Validation\ValidationInterface;

class RelationshipValidator extends LaravelValidator implements ValidationInterface
{
    /**
     * Validation for creating a new User.
     *
     * @var array
     */
    protected $rules = array(
        'title' => 'required|min:5',
        'desc' => 'required|min:5'
    );
}
