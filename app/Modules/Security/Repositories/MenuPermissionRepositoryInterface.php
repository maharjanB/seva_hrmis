<?php

	namespace App\Modules\Security\Repositories;

	use App\Modules\Foundation\Repositories\BaseRepositoryInterface;

	interface MenuPermissionRepositoryInterface extends BaseRepositoryInterface
	{
	}
