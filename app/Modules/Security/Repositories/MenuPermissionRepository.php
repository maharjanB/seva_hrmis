<?php


	namespace App\Modules\Security\Repositories;

	use App\Modules\Foundation\Repositories\AbstractRepository;
	use App\Modules\Security\Entities\MenuPermissionInterface;

	class MenuPermissionRepository extends AbstractRepository implements MenuPermissionRepositoryInterface
	{
		protected $model;

		public function __construct(MenuPermissionInterface $model)
		{
			$this->model = $model;
		}
	}
