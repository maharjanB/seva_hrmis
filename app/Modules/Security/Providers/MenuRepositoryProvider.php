<?php

	namespace App\Modules\Security\Providers;

	use Illuminate\Support\ServiceProvider;
	use App\Modules\Security\Entities\MenuInterface;
	use App\Modules\Security\Repositories\MenuRepository;
	use App\Modules\Security\Repositories\MenuRepositoryInterface;

	class MenuRepositoryProvider extends ServiceProvider
	{
		public function register()
		{
			$this->container = $this->app;
			$this->container->bind(MenuRepositoryInterface::class, function () {
				return new MenuRepository($this->container[ MenuInterface::class ]);
			});
		}
	}
