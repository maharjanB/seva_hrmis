<?php

	namespace App\Modules\Security\Providers;

	use Illuminate\Support\ServiceProvider;
	use App\Modules\Security\Entities\Menu;
	use App\Modules\Security\Entities\MenuInterface;

	class MenuProvider extends ServiceProvider
	{
		public function register()
		{
			$this->container = $this->app;
			$this->container->bind(MenuInterface::class, function () {
				return new Menu();
			});
		}
	}
