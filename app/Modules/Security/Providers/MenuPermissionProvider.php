<?php

	namespace App\Modules\Security\Providers;


	use Illuminate\Support\ServiceProvider;
	use App\Modules\Security\Entities\MenuPermission;
	use App\Modules\Security\Entities\MenuPermissionInterface;

	class MenuPermissionProvider extends ServiceProvider
	{
		public function register()
		{
			$this->container = $this->app;
			$this->container->bind(MenuPermissionInterface::class, function () {
				return new MenuPermission();
			});
		}
	}
