<?php

	namespace App\Modules\Security\Providers;

	use Illuminate\Support\ServiceProvider;
	use App\Modules\Security\Entities\MenuPermissionInterface;
	use App\Modules\Security\Repositories\MenuPermissionRepository;
	use App\Modules\Security\Repositories\MenuPermissionRepositoryInterface;

	class MenuPermissionRepositoryProvider extends ServiceProvider
	{
		public function register()
		{
			$this->container = $this->app;
			$this->container->bind(MenuPermissionRepositoryInterface::class, function () {
				return new MenuPermissionRepository($this->container[ MenuPermissionInterface::class ]);
			});
		}
	}
