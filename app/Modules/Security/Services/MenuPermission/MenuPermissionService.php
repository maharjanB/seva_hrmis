<?php

	namespace App\Modules\Security\Services\MenuPermission;

	use App\Modules\Security\Repositories\MenuPermissionRepositoryInterface;
	use App\Modules\Foundation\Services\AbstractService;
	use App\Modules\Security\Validators\MenuPermissionValidator;

	class MenuPermissionService extends AbstractService
	{
		protected $repository;
		protected $validator;

		public function __construct(MenuPermissionRepositoryInterface $repository)
		{
			$this->repository = $repository;
			$this->validator = new MenuPermissionValidator();
		}

	}
