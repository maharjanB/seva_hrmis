<?php

	namespace App\Modules\Security\Entities;

	use App\Modules\Foundation\Entities\BaseModel;

	class MenuPermission extends BaseModel implements MenuPermissionInterface
	{

		protected $table = 'tbl_menu_permission';

		protected $primaryKey = 'id_menu_permission';

		protected $fillable = ['menu_id', 'role_id', 'permission_id'];

		public function menu()
		{
			return $this->belongsToMany('Modules\Security\Entities\Menu', 'menu_id');
		}


	}
