<?php

    namespace App\Modules\Security\Validators;

    use App\Modules\Foundation\Validation\LaravelValidator;
    use App\Modules\Foundation\Validation\ValidationInterface;

    class MenuPermissionValidator extends LaravelValidator implements ValidationInterface
    {
        /**
         * Validation for creating a new MenuPermission.
         *
         * @var array
         */
        protected $rules = [
            'menu_id'       => 'required',
            'role_id'       => 'required',
            'permission_id' => 'required',
        ];
    }
