/**
 * Created by Dell on 3/9/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.configurations')
        .factory('RelationshipService', function (BaseServerService, $http, API_CONFIG) {

            var apiUrl = API_CONFIG.baseUrl;

            var RelationshipService = function () {
                BaseServerService.constructor.call(this);
            };

            RelationshipService.prototype = new BaseServerService('relationship');


            /**
             * add data to relationship table.
             * @param data
             */
            RelationshipService.prototype.addRelationship = function(data){

                return $http({
                    method: "POST",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    url: apiUrl + '',
                    data: $.param(data)
                });
            };


            /**
             *
             * @param data
             */
            RelationshipService.prototype.editRelationship = function(data){

                return $http({
                    method: "PUT",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    url: apiUrl + 'relationship/' + data['detail[id_relationship]'],
                    data: $.param(data)
                });
            };


            return new RelationshipService();
        });
})();