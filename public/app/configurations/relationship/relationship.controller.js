/**
 * Created by Dell on 3/9/2017.
 */
(function () {
    'use strict';
    angular
        .module('app.configurations')
        .controller('RelationshipController', RelationshipController);

    /* @ngInject */
    function RelationshipController(BaseController, RelationshipService, $filter, $rootScope, API_CONFIG) {

        var vm = this;
        vm.objectList = [];
        vm.primaryId = '';
        vm.table = 'relationship';

        /**
         * End of variables.
         */

        /**
         * Constructor relationshipController.
         * Inherits the parent controller BaseController.
         * Reverse order at Call .. BaseController.call(vm.primaryId, vm.table ); (to defination)
         * @constructor
         */

        function RelationshipController(){
            BaseController.call(this, [vm.table, vm.primaryId]);
        }

        /**
         * Instance of the base controller pulled to the Child Class
         * @type {BaseController}
         */
        RelationshipController.prototype = Object.create(BaseController.prototype);
        /**
         * Initialize the controller.
         *
         * to Use. Can be seperated.
         * @type {relationshipController}
         */
        vm.controller = new RelationshipController();
        /**
         * Pagination. ( Required for all )
         * @type {{filter: string, limit: string, order: string, page: number, total: string}}
         */
        vm.controller.query = {
            filter: '',
            limit: API_CONFIG.dbLimit,
            order: '-id',
            page: 1,
            total: '',
            lastPageNo: ''
        };


        /** get the data of relationship **/
        vm.controller.setObjectList = function(){
            vm.controller.getList(vm.controller.query).then(function(response){
                if(response.status === 200 || response.status === 'success'){
                    vm.objectList = response.data;
                    vm.controller.query.total = response.total;
                    vm.controller.query.lastPageNo = Math.ceil(vm.controller.query.total / vm.controller.query.limit);
                }
            })
        };
        vm.controller.setObjectList();


        vm.addItem = {
            'type': 'Create relationship',
            'relationshipId': '',
            'relationshipTitle': '',
            'relationshipDesc': ''
        };


        vm.resetAddItem = function(){
            vm.addItem = {
                'type': 'Create relationship',
                'relationshipId': '',
                'relationshipTitle': '',
                'relationshipDesc': ''
            };
        };

        vm.relationshipAddItem = function(){
            var obj = {'title': vm.addItem.relationshipTitle,
                'desc': vm.addItem.relationshipDesc
            };
            if(vm.addItem.type === 'Create relationship'){
                RelationshipService.postObject(obj).then(function(response){
                    if(response.status === 200){
                        vm.objectList.push(response.data.data);
                    }
                    else{
                        Materialize.toast("Error", 2000);
                    }
                    // reset add items.
                    vm.resetAddItem();
                });
            }
            else if(vm.addItem.type === 'Edit relationship'){
                var obj = {
                    'id_relationship' : vm.addItem.relationshipId,
                    'title': vm.addItem.relationshipTitle,
                    'desc': vm.addItem.relationshipDesc,
                };
                RelationshipService.editObject(obj.id_relationship,obj).then(function(response){

                    if(response.data.data){
                        //editing in view
                        var relationship = $filter('filter')(vm.objectList, {id_relationship: vm.addItem.relationshipId}, true)[0];
                        relationship.title = vm.addItem.relationshipTitle;
                        relationship.desc = vm.addItem.relationshipDesc;
                    }

                    vm.resetAddItem();
                })
            }
        };


        /**
         *
         * @param relationship
         */
        vm.setEditData = function(relationship){
            vm.addItem.type = 'Edit relationship';
            vm.addItem.relationshipId = relationship.id_relationship;
            vm.addItem.relationshipTitle = relationship.title;
            vm.addItem.relationshipDesc = relationship.desc;
        };


        /**Initialize dialog box**/
        $rootScope.$on('$includeContentLoaded', function () {

            $('.modal-trigger').leanModal({
                    dismissible: true, // Modal can be dismissed by clicking outside of the modal
                    opacity: 0.5, // Opacity of modal background
                    in_duration: 200, // Transition in duration
                    out_duration: 200, // Transition out duration
                    starting_top: '4%', // Starting top style attribute
                    ending_top: '10%', // Ending top style attribute
                    ready: function () {
                    }, // Callback for Modal open
                    complete: function () {
                    } // Callback for Modal close
                }
            );
        });
    }
})();