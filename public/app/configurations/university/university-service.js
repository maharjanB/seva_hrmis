/**
 * Created by Dell on 3/9/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.configurations')
        .factory('UniversityService', function (BaseServerService, $http, API_CONFIG) {

            var apiUrl = API_CONFIG.baseUrl;

            var UniversityService = function () {
                BaseServerService.constructor.call(this);
            };

            UniversityService.prototype = new BaseServerService('university');


            /**
             * add data to university table.
             * @param data
             */
            UniversityService.prototype.addUniversity = function(data){

                return $http({
                    method: "POST",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    url: apiUrl + '',
                    data: $.param(data)
                });
            };


            /**
             *
             * @param data
             */
            UniversityService.prototype.editUniversity = function(data){

                return $http({
                    method: "PUT",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    url: apiUrl + 'university/' + data['detail[id_university]'],
                    data: $.param(data)
                });
            };


            return new UniversityService();
        });
})();