/**
 * Created by Dell on 3/9/2017.
 */
(function () {
    'use strict';
    angular
        .module('app.configurations')
        .controller('UniversityController', UniversityController);

    /* @ngInject */
    function UniversityController(BaseController, UniversityService, $filter, $rootScope, API_CONFIG) {

        var vm = this;
        vm.objectList = [];
        vm.primaryId = '';
        vm.table = 'university';

        /**
         * End of variables.
         */

        /**
         * Constructor UniversityController.
         * Inherits the parent controller BaseController.
         * Reverse order at Call .. BaseController.call(vm.primaryId, vm.table ); (to defination)
         * @constructor
         */

        function UniversityController(){
            BaseController.call(this, [vm.table, vm.primaryId]);
        }

        /**
         * Instance of the base controller pulled to the Child Class
         * @type {BaseController}
         */
        UniversityController.prototype = Object.create(BaseController.prototype);
        /**
         * Initialize the controller.
         *
         * to Use. Can be seperated.
         * @type {universityController}
         */
        vm.controller = new UniversityController();
        /**
         * Pagination. ( Required for all )
         * @type {{filter: string, limit: string, order: string, page: number, total: string}}
         */
        vm.controller.query = {
            filter: '',
            limit: API_CONFIG.dbLimit,
            order: '-id',
            page: 1,
            total: '',
            lastPageNo: ''
        };


        /** get the data of university **/
        vm.controller.setObjectList = function(){
            vm.controller.getList(vm.controller.query).then(function(response){
                if(response.status === 200 || response.status === 'success'){
                    vm.objectList = response.data;
                    vm.controller.query.total = response.total;
                    vm.controller.query.lastPageNo = Math.ceil(vm.controller.query.total / vm.controller.query.limit);
                }
            })
        };
        vm.controller.setObjectList();


        vm.addItem = {
            'type': 'Create university',
            'universityId': '',
            'universityTitle': '',
            'universityDesc': ''
        };


        vm.resetAddItem = function(){
            vm.addItem = {
                'type': 'Create university',
                'universityId': '',
                'universityTitle': '',
                'universityDesc': ''
            };
        };

        vm.universityAddItem = function(){
            var obj = {'title': vm.addItem.universityTitle,
                'desc': vm.addItem.universityDesc
            };
            if(vm.addItem.type === 'Create university'){
                UniversityService.postObject(obj).then(function(response){
                    if(response.status === 200){
                        vm.objectList.push(response.data.data);
                    }
                    else{
                        Materialize.toast("Error", 2000);
                    }
                    // reset add items.
                    vm.resetAddItem();
                });
            }
            else if(vm.addItem.type === 'Edit university'){
                var obj = {
                    'id_university' : vm.addItem.universityId,
                    'title': vm.addItem.universityTitle,
                    'desc': vm.addItem.universityDesc,
                };
                UniversityService.editObject(obj.id_university,obj).then(function(response){

                    if(response.data.data){
                        //editing in view
                        var university = $filter('filter')(vm.objectList, {id_university: vm.addItem.universityId}, true)[0];
                        university.title = vm.addItem.universityTitle;
                        university.desc = vm.addItem.universityDesc;
                    }

                    vm.resetAddItem();
                })
            }
        };


        /**
         *
         * @param university
         */
        vm.setEditData = function(university){
            vm.addItem.type = 'Edit university';
            vm.addItem.universityId = university.id_university;
            vm.addItem.universityTitle = university.title;
            vm.addItem.universityDesc = university.desc;
        };


        /**Initialize dialog box**/
        $rootScope.$on('$includeContentLoaded', function () {

            $('.modal-trigger').leanModal({
                    dismissible: true, // Modal can be dismissed by clicking outside of the modal
                    opacity: 0.5, // Opacity of modal background
                    in_duration: 200, // Transition in duration
                    out_duration: 200, // Transition out duration
                    starting_top: '4%', // Starting top style attribute
                    ending_top: '10%', // Ending top style attribute
                    ready: function () {
                    }, // Callback for Modal open
                    complete: function () {
                    } // Callback for Modal close
                }
            );
        });
    }
})();