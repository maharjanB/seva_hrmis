/**
 * Created by Dell on 3/9/2017.
 */
(function () {
    'use strict';
    angular
        .module('app.configurations')
        .controller('MaritalstatusController', MaritalstatusController);

    /* @ngInject */
    function MaritalstatusController(BaseController, MaritalstatusService, $filter, $rootScope, API_CONFIG) {

        var vm = this;
        vm.objectList = [];
        vm.primaryId = '';
        vm.table = 'maritalstatus';

        /**
         * End of variables.
         */

        /**
         * Constructor MaritalstatusController.
         * Inherits the parent controller BaseController.
         * Reverse order at Call .. BaseController.call(vm.primaryId, vm.table ); (to defination)
         * @constructor
         */

        function MaritalstatusController(){
            BaseController.call(this, [vm.table, vm.primaryId]);
        }

        /**
         * Instance of the base controller pulled to the Child Class
         * @type {BaseController}
         */
        MaritalstatusController.prototype = Object.create(BaseController.prototype);
        /**
         * Initialize the controller.
         *
         * to Use. Can be seperated.
         * @type {MaritalstatusController}
         */
        vm.controller = new MaritalstatusController();
        /**
         * Pagination. ( Required for all )
         * @type {{filter: string, limit: string, order: string, page: number, total: string}}
         */
        vm.controller.query = {
            filter: '',
            limit: API_CONFIG.dbLimit,
            order: '-id',
            page: 1,
            total: '',
            lastPageNo: ''
        };


        /** get the data of maritalstatus **/
        vm.controller.setObjectList = function(){
            vm.controller.getList(vm.controller.query).then(function(response){
                if(response.status === 200 || response.status === 'success'){
                    vm.objectList = response.data;
                    vm.controller.query.total = response.total;
                    vm.controller.query.lastPageNo = Math.ceil(vm.controller.query.total / vm.controller.query.limit);
                }
            })
        };
        vm.controller.setObjectList();


        vm.addItem = {
            'type': 'Create maritalstatus',
            'maritalstatusId': '',
            'maritalstatusTitle': '',
            'maritalstatusDesc': ''
        };


        vm.resetAddItem = function(){
            vm.addItem = {
                'type': 'Create maritalstatus',
                'maritalstatusId': '',
                'maritalstatusTitle': '',
                'maritalstatusDesc': ''
            };
        };

        vm.maritalstatusAddItem = function(){
            var obj = {'title': vm.addItem.maritalstatusTitle,
                'desc': vm.addItem.maritalstatusDesc
            };
            if(vm.addItem.type === 'Create maritalstatus'){
                MaritalstatusService.postObject(obj).then(function(response){
                    if(response.status === 200){
                        vm.objectList.push(response.data.data);
                    }
                    else{
                        Materialize.toast("Error", 2000);
                    }
                    // reset add items.
                    vm.resetAddItem();
                });
            }
            else if(vm.addItem.type === 'Edit maritalstatus'){
                var obj = {
                    'id_maritalstatus' : vm.addItem.maritalstatusId,
                    'title': vm.addItem.maritalstatusTitle,
                    'desc': vm.addItem.maritalstatusDesc,
                };
                MaritalstatusService.editObject(obj.id_maritalstatus,obj).then(function(response){

                    if(response.data.data){
                        //editing in view
                        var maritalstatus = $filter('filter')(vm.objectList, {id_maritalstatus: vm.addItem.maritalstatusId}, true)[0];
                        maritalstatus.title = vm.addItem.maritalstatusTitle;
                        maritalstatus.desc = vm.addItem.maritalstatusDesc;
                    }

                    vm.resetAddItem();
                })
            }
        };


        /**
         *
         * @param maritalstatus
         */
        vm.setEditData = function(maritalstatus){
            vm.addItem.type = 'Edit maritalstatus';
            vm.addItem.maritalstatusId = maritalstatus.id_maritalstatus;
            vm.addItem.maritalstatusTitle = maritalstatus.title;
            vm.addItem.maritalstatusDesc = maritalstatus.desc;
        };


        /**Initialize dialog box**/
        $rootScope.$on('$includeContentLoaded', function () {

            $('.modal-trigger').leanModal({
                    dismissible: true, // Modal can be dismissed by clicking outside of the modal
                    opacity: 0.5, // Opacity of modal background
                    in_duration: 200, // Transition in duration
                    out_duration: 200, // Transition out duration
                    starting_top: '4%', // Starting top style attribute
                    ending_top: '10%', // Ending top style attribute
                    ready: function () {
                    }, // Callback for Modal open
                    complete: function () {
                    } // Callback for Modal close
                }
            );
        });
    }
})();