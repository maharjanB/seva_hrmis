/**
 * Created by Dell on 3/9/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.configurations')
        .factory('MaritalstatusService', function (BaseServerService, $http, API_CONFIG) {

            var apiUrl = API_CONFIG.baseUrl;

            var MaritalstatusService = function () {
                BaseServerService.constructor.call(this);
            };

            MaritalstatusService.prototype = new BaseServerService('maritalstatus');


            /**
             * add data to maritalstatus table.
             * @param data
             */
            MaritalstatusService.prototype.addMaritalstatus = function(data){

                return $http({
                    method: "POST",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    url: apiUrl + '',
                    data: $.param(data)
                });
            };


            /**
             *
             * @param data
             */
            MaritalstatusService.prototype.editMaritalstatus = function(data){

                return $http({
                    method: "PUT",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    url: apiUrl + 'maritalstatus/' + data['detail[id_maritalstatus]'],
                    data: $.param(data)
                });
            };


            return new MaritalstatusService();
        });
})();