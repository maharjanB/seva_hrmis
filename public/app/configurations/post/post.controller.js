(function () {
    'use strict';
    angular
        .module('app.configurations')
        .controller('PostController', PostController);

    /* @ngInject */
    function PostController(BaseController, PostService, $filter, $rootScope, API_CONFIG) {

        var vm = this;
        vm.objectList = [];
        vm.primaryId = '';
        vm.table = 'post';

        /**
         * End of variables.
         */

        /**
         * Constructor postController.
         * Inherits the parent controller BaseController.
         * Reverse order at Call .. BaseController.call(vm.primaryId, vm.table ); (to defination)
         * @constructor
         */

        function PostController(){
            BaseController.call(this, [vm.table, vm.primaryId]);
        }

        /**
         * Instance of the base controller pulled to the Child Class
         * @type {BaseController}
         */
        PostController.prototype = Object.create(BaseController.prototype);
        /**
         * Initialize the controller.
         *
         * to Use. Can be seperated.
         * @type {postController}
         */
        vm.controller = new PostController();
        /**
         * Pagination. ( Required for all )
         * @type {{filter: string, limit: string, order: string, page: number, total: string}}
         */
        vm.controller.query = {
            filter: '',
            limit: API_CONFIG.dbLimit,
            order: '-id',
            page: 1,
            total: '',
            lastPageNo: ''
        };


        /** get the data of post **/
        vm.controller.setObjectList = function(){
            vm.controller.getList(vm.controller.query).then(function(response){
                if(response.status === 200 || response.status === 'success'){
                    vm.objectList = response.data;
                    vm.controller.query.total = response.total;
                    vm.controller.query.lastPageNo = Math.ceil(vm.controller.query.total / vm.controller.query.limit);
                }
            })
        };
        vm.controller.setObjectList();


        vm.addItem = {
            'type': 'Create post',
            'postId': '',
            'postTitle': '',
            'postDesc':''
        };


        vm.resetAddItem = function(){
            vm.addItem = {
                'type': 'Create post',
                'postId': '',
                'postTitle': '',
                'postDesc': ''
            };
        };

        vm.postAddItem = function(){
            var obj = {     'title': vm.addItem.postTitle,
                            'desc': vm.addItem.postDesc

                        };
            if(vm.addItem.type === 'Create post'){
                PostService.postObject(obj).then(function(response){
                    if(response.status === 200){
                        vm.objectList.push(response.data.data);
                    }
                    else{
                        Materialize.toast("Error", 2000);
                    }
                    // reset add items.
                    vm.resetAddItem();
                });
            }
            else if(vm.addItem.type === 'Edit post'){
                var obj = {
                    'id_post' : vm.addItem.postId,
                    'title': vm.addItem.postTitle,
                    'desc': vm.addItem.postDesc
                };
                PostService.editObject(obj.id_post,obj).then(function(response){

                    if(response.data.data){
                        //editing in view
                        var post = $filter('filter')(vm.objectList, {id_post: vm.addItem.postId}, true)[0];
                        post.title = vm.addItem.postTitle;
                        post.desc = vm.addItem.postDesc;
                    }

                    vm.resetAddItem();
                })
            }
        };


        /**
         *
         * @param post
         */
        vm.setEditData = function(post){
            vm.addItem.type = 'Edit post';
            vm.addItem.postId = post.id_post;
            vm.addItem.postTitle = post.title;
            vm.addItem.postDesc = post.desc;

        };


        /**Initialize dialog box**/
        $rootScope.$on('$includeContentLoaded', function () {

            $('.modal-trigger').leanModal({
                    dismissible: true, // Modal can be dismissed by clicking outside of the modal
                    opacity: 0.5, // Opacity of modal background
                    in_duration: 200, // Transition in duration
                    out_duration: 200, // Transition out duration
                    starting_top: '4%', // Starting top style attribute
                    ending_top: '10%', // Ending top style attribute
                    ready: function () {
                    }, // Callback for Modal open
                    complete: function () {
                    } // Callback for Modal close
                }
            );
        });
    }
})();