(function () {
    'use strict';

    angular
        .module('app.configurations')
        .factory('PostService', function (BaseServerService, $http, API_CONFIG) {

            var apiUrl = API_CONFIG.baseUrl;

            var PostService = function () {
                BaseServerService.constructor.call(this);
            };

            PostService.prototype = new BaseServerService('post');


            /**
             * add data to post table.
             * @param data
             */
            PostService.prototype.addPost = function(data){

                return $http({
                    method: "POST",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    url: apiUrl + '',
                    data: $.param(data)
                });
            };


            /**
             *
             * @param data
             */
            PostService.prototype.editPost = function(data){

                return $http({
                    method: "PUT",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    url: apiUrl + 'post/' + data['detail[id_post]'],
                    data: $.param(data)
                });
            };


            return new PostService();
        });
})();