(function () {
    'use strict';
    angular
        .module('app.configurations')
        .controller('RoleController', RoleController);

    /* @ngInject */
    function RoleController(BaseController, RoleService, $filter, $rootScope, API_CONFIG) {

        var vm = this;
        vm.objectList = [];
        vm.primaryId = '';
        vm.table = 'role';

        /**
         * End of variables.
         */

        /**
         * Constructor roleController.
         * Inherits the parent controller BaseController.
         * Reverse order at Call .. BaseController.call(vm.primaryId, vm.table ); (to defination)
         * @constructor
         */

        function RoleController(){
            BaseController.call(this, [vm.table, vm.primaryId]);
        }

        /**
         * Instance of the base controller pulled to the Child Class
         * @type {BaseController}
         */
        RoleController.prototype = Object.create(BaseController.prototype);
        /**
         * Initialize the controller.
         *
         * to Use. Can be seperated.
         * @type {roleController}
         */
        vm.controller = new RoleController();
        /**
         * Pagination. ( Required for all )
         * @type {{filter: string, limit: string, order: string, page: number, total: string}}
         */
        vm.controller.query = {
            filter: '',
            limit: API_CONFIG.dbLimit,
            order: '-id',
            page: 1,
            total: '',
            lastPageNo: ''
        };


        /** get the data of role **/
        vm.controller.setObjectList = function(){
            vm.controller.getList(vm.controller.query).then(function(response){
                if(response.status === 200 || response.status === 'success'){
                    vm.objectList = response.data;
                    vm.controller.query.total = response.total;
                    vm.controller.query.lastPageNo = Math.ceil(vm.controller.query.total / vm.controller.query.limit);
                }
            })
        };
        vm.controller.setObjectList();


        vm.addItem = {
            'type': 'Create role',
            'roleId': '',
            'roleTitle': '',
            'roleDesc':''
        };


        vm.resetAddItem = function(){
            vm.addItem = {
                'type': 'Create role',
                'roleId': '',
                'roleTitle': '',
                'roleDesc': ''
            };
        };

        vm.roleAddItem = function(){
            var obj= {     'title': vm.addItem.roleTitle,
                            'desc': vm.addItem.roleDesc

                        };
            if(vm.addItem.type === 'Create role'){
                RoleService.postObject(obj).then(function(response){
                    if(response.status === 200){
                        vm.objectList.push(response.data.data);
                    }
                    else{
                        Materialize.toast("Error", 2000);
                    }
                    // reset add items.
                    vm.resetAddItem();
                });
            }
            else if(vm.addItem.type === 'Edit role'){
                var obj = {
                    'id_role' : vm.addItem.roleId,
                    'title': vm.addItem.roleTitle,
                    'desc': vm.addItem.roleDesc
                };
                RoleService.editObject(obj.id_role,obj).then(function(response){

                    if(response.data.data){
                        //editing in view
                        var role = $filter('filter')(vm.objectList, {id_role: vm.addItem.roleId}, true)[0];
                        role.title = vm.addItem.roleTitle;
                        role.desc = vm.addItem.roleDesc;
                    }

                    vm.resetAddItem();
                })
            }
        };


        /**
         *
         * @param role
         */
        vm.setEditData = function(role){
            vm.addItem.type = 'Edit role';
            vm.addItem.roleId = role.id_role;
            vm.addItem.roleTitle = role.title;
            vm.addItem.roleDesc = role.desc;

        };


        /**Initialize dialog box**/
        $rootScope.$on('$includeContentLoaded', function () {

            $('.modal-trigger').leanModal({
                    dismissible: true, // Modal can be dismissed by clicking outside of the modal
                    opacity: 0.5, // Opacity of modal background
                    in_duration: 200, // Transition in duration
                    out_duration: 200, // Transition out duration
                    starting_top: '4%', // Starting top style attribute
                    ending_top: '10%', // Ending top style attribute
                    ready: function () {
                    }, // Callback for Modal open
                    complete: function () {
                    } // Callback for Modal close
                }
            );
        });
    }
})();