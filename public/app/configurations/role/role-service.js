(function () {
    'use strict';

    angular
        .module('app.configurations')
        .factory('RoleService', function (BaseServerService, $http, API_CONFIG) {

            var apiUrl = API_CONFIG.baseUrl;

            var RoleService = function () {
                BaseServerService.constructor.call(this);
            };

            RoleService.prototype = new BaseServerService('role');


            /**
             * add data to role table.
             * @param data
             */
            RoleService.prototype.addRole = function(data){

                return $http({
                    method: "POST",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    url: apiUrl + '',
                    data: $.param(data)
                });
            };


            /**
             *
             * @param data
             */
            RoleService.prototype.editRole = function(data){

                return $http({
                    method: "PUT",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    url: apiUrl + '/' + data['detail[id_role]'],
                    data: $.param(data)
                });
            };


            return new RoleService();
        });
})();