(function () {
    'use strict';

    angular
        .module('app.configurations')
        .factory('EducationService', function (BaseServerService, $http, API_CONFIG) {

            var apiUrl = API_CONFIG.baseUrl;

            var EducationService = function () {
                BaseServerService.constructor.call(this);
            };

            EducationService.prototype = new BaseServerService('education');


            /**
             * add data to education table.
             * @param data
             */
            EducationService.prototype.addEducation = function(data){

                return $http({
                    method: "POST",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    url: apiUrl + '',
                    data: $.param(data)
                });
            };


            /**
             *
             * @param data
             */
            EducationService.prototype.editEducation = function(data){

                return $http({
                    method: "PUT",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    url: apiUrl + 'education/' + data['detail[id_education]'],
                    data: $.param(data)
                });
            };


            return new EducationService();
        });
})();