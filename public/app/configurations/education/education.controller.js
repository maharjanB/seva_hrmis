(function () {
    'use strict';
    angular
        .module('app.configurations')
        .controller('EducationController', EducationController);

    /* @ngInject */
    function EducationController(BaseController, EducationService, $filter, $rootScope, API_CONFIG) {

        var vm = this;
        vm.objectList = [];
        vm.primaryId = '';
        vm.table = 'education';

        /**
         * End of variables.
         */

        /**
         * Constructor educationController.
         * Inherits the parent controller BaseController.
         * Reverse order at Call .. BaseController.call(vm.primaryId, vm.table ); (to defination)
         * @constructor
         */

        function EducationController(){
            BaseController.call(this, [vm.table, vm.primaryId]);
        }

        /**
         * Instance of the base controller pulled to the Child Class
         * @type {BaseController}
         */
        EducationController.prototype = Object.create(BaseController.prototype);
        /**
         * Initialize the controller.
         *
         * to Use. Can be seperated.
         * @type {educationController}
         */
        vm.controller = new EducationController();
        /**
         * Pagination. ( Required for all )
         * @type {{filter: string, limit: string, order: string, page: number, total: string}}
         */
        vm.controller.query = {
            filter: '',
            limit: API_CONFIG.dbLimit,
            order: '-id',
            page: 1,
            total: '',
            lastPageNo: ''
        };


        /** get the data of education **/
        vm.controller.setObjectList = function(){
            vm.controller.getList(vm.controller.query).then(function(response){
                if(response.status === 200 || response.status === 'success'){
                    vm.objectList = response.data;
                    vm.controller.query.total = response.total;
                    vm.controller.query.lastPageNo = Math.ceil(vm.controller.query.total / vm.controller.query.limit);
                }
            })
        };
        vm.controller.setObjectList();


        vm.addItem = {
            'type': 'Create education',
            'educationId': '',
            'educationTitle': '',
            'educationDesc': ''
        };


        vm.resetAddItem = function(){
            vm.addItem = {
                'type': 'Create education',
                'educationId': '',
                 'educationTitle': '',
                'educationDesc': ''
            };
        };

        vm.educationAddItem = function(){
            var obj = {
                            'title': vm.addItem.educationTitle,
                            'description': vm.addItem.educationDescription,
                        };
            if(vm.addItem.type === 'Create education'){
                EducationService.postObject(obj).then(function(response){
                    if(response.status === 200){
                        vm.objectList.push(response.data.data);
                    }
                    else{
                        Materialize.toast("Error", 2000);
                    }
                    // reset add items.
                    vm.resetAddItem();
                });
            }
            else if(vm.addItem.type === 'Edit education'){
                var obj = {
                    'id_education' : vm.addItem.educationId,
                    'title': vm.addItem.educationTitle,
                    'description': vm.addItem.educationDescription,
                };
                EducationService.editObject(obj.id_education,obj).then(function(response){

                    if(response.data.data){
                        //editing in view
                        var education = $filter('filter')(vm.objectList, {id_education: vm.addItem.educationId}, true)[0];
                        education.title = vm.addItem.educationTitle;
                        education.description = vm.addItem.educationDesc;

                    }

                    vm.resetAddItem();
                })
            }
        };


        /**
         *
         * @param education
         */
        vm.setEditData = function(education){
            vm.addItem.type = 'Edit education';
            vm.addItem.educationId = education.id_education;
            vm.addItem.educationTitle=education.title;
            vm.addItem.educationDesc = education.desc;
        }


        /**Initialize dialog box**/
        $rootScope.$on('$includeContentLoaded', function () {

            $('.modal-trigger').leanModal({
                    dismissible: true, // Modal can be dismissed by clicking outside of the modal
                    opacity: 0.5, // Opacity of modal background
                    in_duration: 200, // Transition in duration
                    out_duration: 200, // Transition out duration
                    starting_top: '4%', // Starting top style attribute
                    ending_top: '10%', // Ending top style attribute
                    ready: function () {
                    }, // Callback for Modal open
                    complete: function () {
                    } // Callback for Modal close
                }
            );
        });
    }
})();