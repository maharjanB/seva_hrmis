/**
 * Created by Dell on 3/9/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.configurations')
        .factory('SkillService', function (BaseServerService, $http, API_CONFIG) {

            var apiUrl = API_CONFIG.baseUrl;

            var SkillService = function () {
                BaseServerService.constructor.call(this);
            };

            SkillService.prototype = new BaseServerService('skill');


            /**
             * add data to Skill table.
             * @param data
             */
            SkillService.prototype.addSkill = function(data){

                return $http({
                    method: "POST",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    url: apiUrl + '',
                    data: $.param(data)
                });
            };


            /**
             *
             * @param data
             */
            SkillService.prototype.editSkill = function(data){

                return $http({
                    method: "PUT",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    url: apiUrl + 'skill/' + data['detail[id_skill]'],
                    data: $.param(data)
                });
            };


            return new SkillService();
        });
})();