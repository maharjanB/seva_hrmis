(function () {
    'use strict';

    angular
        .module('app.configurations')
        .factory('BloodGroupService', function (BaseServerService, $http, API_CONFIG) {

            var apiUrl = API_CONFIG.baseUrl;

            var BloodGroupService = function () {
                BaseServerService.constructor.call(this);
            };

            BloodGroupService.prototype = new BaseServerService('bloodGroup');


            /**
             * add data to bloodGroup table.
             * @param data
             */
            BloodGroupService.prototype.addBloodGroup = function(data){

                return $http({
                    method: "POST",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    url: apiUrl + '',
                    data: $.param(data)
                });
            };


            /**
             *
             * @param data
             */
            BloodGroupService.prototype.editBloodGroup = function(data){

                return $http({
                    method: "PUT",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    url: apiUrl + 'bloodGroup/' + data['detail[id_blood_group]'],
                    data: $.param(data)
                });
            };


            return new BloodGroupService();
        });
})();