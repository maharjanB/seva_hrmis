(function () {
    'use strict';
    angular
        .module('app.configurations')
        .controller('ReligionController', ReligionController);

    /* @ngInject */
    function ReligionController(BaseController, ReligionService, $filter, $rootScope, API_CONFIG) {

        var vm = this;
        vm.objectList = [];
        vm.primaryId = '';
        vm.table = 'religion';

        /**
         * End of variables.
         */

        /**
         * Constructor religionController.
         * Inherits the parent controller BaseController.
         * Reverse order at Call .. BaseController.call(vm.primaryId, vm.table ); (to defination)
         * @constructor
         */

        function ReligionController(){
            BaseController.call(this, [vm.table, vm.primaryId]);
        }

        /**
         * Instance of the base controller pulled to the Child Class
         * @type {BaseController}
         */
        ReligionController.prototype = Object.create(BaseController.prototype);
        /**
         * Initialize the controller.
         *
         * to Use. Can be seperated.
         * @type {religionController}
         */
        vm.controller = new ReligionController();
        /**
         * Pagination. ( Required for all )
         * @type {{filter: string, limit: string, order: string, page: number, total: string}}
         */
        vm.controller.query = {
            filter: '',
            limit: API_CONFIG.dbLimit,
            order: '-id',
            page: 1,
            total: '',
            lastPageNo: ''
        };


        /** get the data of religion **/
        vm.controller.setObjectList = function(){
            vm.controller.getList(vm.controller.query).then(function(response){
                if(response.status === 200 || response.status === 'success'){
                    vm.objectList = response.data;
                    vm.controller.query.total = response.total;
                    vm.controller.query.lastPageNo = Math.ceil(vm.controller.query.total / vm.controller.query.limit);
                }
            })
        };
        vm.controller.setObjectList();


        vm.addItem = {
            'type': 'Create religion',
            'religionId': '',
            'religionTitle': ''
        };


        vm.resetAddItem = function(){
            vm.addItem = {
                'type': 'Create religion',
                'religionId': '',
                'religionTitle': ''
            };
        };

        vm.religionAddItem = function(){
            var obj = {
                'desc': vm.addItem.religionTitle,
            };
            if(vm.addItem.type === 'Create religion'){
                ReligionService.postObject(obj).then(function(response){
                    if(response.status === 200){
                        vm.objectList.push(response.data.data);
                    }
                    else{
                        Materialize.toast("Error", 2000);
                    }
                    // reset add items.
                    vm.resetAddItem();
                });
            }
            else if(vm.addItem.type === 'Edit religion'){
                var obj = {
                    'id_religion' : vm.addItem.religionId,
                    'desc': vm.addItem.religionTitle
                };
                ReligionService.editObject(obj.id_religion,obj).then(function(response){

                    if(response.data.data){
                        //editing in view
                        var religion = $filter('filter')(vm.objectList, {id_religion: vm.addItem.religionId}, true)[0];
                        religion.desc = vm.addItem.religionTitle;
                    }

                    vm.resetAddItem();
                })
            }
        };


        /**
         *
         * @param religion
         */
        vm.setEditData = function(religion){
            vm.addItem.type = 'Edit religion';
            vm.addItem.religionId = religion.id_religion;
            vm.addItem.religionTitle = religion.desc;
        };


        /**Initialize dialog box**/
        $rootScope.$on('$includeContentLoaded', function () {

            $('.modal-trigger').leanModal({
                    dismissible: true, // Modal can be dismissed by clicking outside of the modal
                    opacity: 0.5, // Opacity of modal background
                    in_duration: 200, // Transition in duration
                    out_duration: 200, // Transition out duration
                    starting_top: '4%', // Starting top style attribute
                    ending_top: '10%', // Ending top style attribute
                    ready: function () {
                    }, // Callback for Modal open
                    complete: function () {
                    } // Callback for Modal close
                }
            );
        });
    }
})();