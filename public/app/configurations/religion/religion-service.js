(function () {
    'use strict';

    angular
        .module('app.configurations')
        .factory('ReligionService', function (BaseServerService, $http, API_CONFIG) {

            var apiUrl = API_CONFIG.baseUrl;

            var ReligionService = function () {
                BaseServerService.constructor.call(this);
            };

            ReligionService.prototype = new BaseServerService('religion');


            /**
             * add data to religion table.
             * @param data
             */
            ReligionService.prototype.addReligion = function(data){

                return $http({
                    method: "POST",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    url: apiUrl + '',
                    data: $.param(data)
                });
            };


            /**
             *
             * @param data
             */
            ReligionService.prototype.editReligion = function(data){

                return $http({
                    method: "PUT",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    url: apiUrl + 'religion/' + data['detail[id_religion]'],
                    data: $.param(data)
                });
            };


            return new ReligionService();
        });
})();