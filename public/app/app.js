/**
 * Inject any modules in the application here.
 */
(function(){
  'use strict';

    angular.module('app', [
        'app.foundation',
        'app.navigation',
        'app.authentication',
       'app.configurations',
        'app.dashboard',
        'ui.bootstrap'
        //'app.print',
        //'app.csvToJson',
    ])
      .constant('API_CONFIG', {
        'url': 'http://triangular-api.oxygenna.com/',
        'baseUrl': window.location.origin+'/seva_hrmis/public/api/v2/',
        ///*'baseUrl': 'http://rosiav2.local/api/v2/',*/
        'dbLimit': '50'
      });

})();
