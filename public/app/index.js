'use strict';

/** Include any foreign dependency here ***/

var rosiaApp = angular.module('rosiaApp', ['ui.router', 'app', 'pasvaz.bindonce', 'sly', 'ngMap', 'ngSanitize', 'ngCsv', 'ngCookies']);

rosiaApp.config(function ($stateProvider, $urlRouterProvider, $httpProvider) {

    $urlRouterProvider.when("", "/login");
    $urlRouterProvider.when("/", "/login");

    // For any unmatched url, send to /route1
    $urlRouterProvider.otherwise("/login");

    $stateProvider
        .state('authentication', {
            abstract: true,
            templateUrl: 'app/authentication/layouts/authentication.tmpl.html'
        })
        .state('authentication.login', {
            url: '/login',
            templateUrl: 'app/authentication/login/login.tmpl.html',
            controller: 'LoginController',
            controllerAs: 'vm'
        });

    $stateProvider
        .state('app', {
            abstract: true,
            url: '/app',
            templateUrl: "app/views/layout.html",
        })
        .state('app.user', {
            url: '/user-account',
            // loaded into ui-view of parent's template
            templateUrl: 'app/configurations/user/userAccount/user-account.tmpl.html',
            controller: 'userAccountController',
            controllerAs: 'vm'
        })

        .state('app.dashboard', {
            url: '/dashboard',
            templateUrl: 'app/dashboard/dashboard.tmpl.html',
            controller: 'DashboardController',
            controllerAs: 'vm'
        })
    .state('app.country', {
        url: '/country',
        templateUrl: 'app/configurations/country/country.tmpl.html',
        controller: 'CountryController',
        controllerAs: 'vm'
    })
        .state('app.university', {
            url: '/university',
            templateUrl: 'app/configurations/university/university.tmpl.html',
            controller: 'UniversityController',
            controllerAs: 'vm'
        })
        .state('app.relationship', {
            url: '/relationship',
            templateUrl: 'app/configurations/relationship/relationship.tmpl.html',
            controller: 'RelationshipController',
            controllerAs: 'vm'
        })
        .state('app.maritalstatus', {
            url: '/maritalstatus',
            templateUrl: 'app/configurations/maritalstatus/maritalstatus.tmpl.html',
            controller: 'MaritalstatusController',
            controllerAs: 'vm'
        })
        .state('app.skill', {
            url: '/skill',
            templateUrl: 'app/configurations/skill/skill.tmpl.html',
            controller: 'SkillController',
            controllerAs: 'vm'
    })
        .state('app.education', {
            url: '/education',
            templateUrl: 'app/configurations/education/education.tmpl.html',
            controller: 'EducationController',
            controllerAs: 'vm'
        })
        .state('app.bloodGroup', {
            url: '/bloodGroup',
            templateUrl: 'app/configurations/bloodGroup/bloodGroup.tmpl.html',
            controller: 'BloodGroupController',
            controllerAs: 'vm'

    })
        .state('app.post', {
        url: '/post',
        templateUrl: 'app/configurations/post/post.tmpl.html',
        controller: 'PostController',
        controllerAs: 'vm'
    })
        .state('app.role', {
            url: '/role',
            templateUrl: 'app/configurations/role/role.tmpl.html',
            controller: 'RoleController',
            controllerAs: 'vm'
        })
        .state('app.religion', {
            url: '/religion',
            templateUrl: 'app/configurations/religion/religion.tmpl.html',
            controller: 'ReligionController',
            controllerAs: 'vm'
        });



    /*
     //Interceptor implementation
     */
    $httpProvider.interceptors.push('APIInterceptor');


});


