
<?php

return [
    'permissionKeyword'=>[
        "READ" => "1",
        "WRITE" => "2",
        "DELETE" => "3",
        "VISIBLE" => "4",
    ],
    'menuWiseControllerMethodPermission'=>[
        "index" => "READ",
        "show" => "READ",
        "create" => "WRITE",
        "store" => "WRITE",
        "edit" => "WRITE",
        "update" => "WRITE",
        "destroy" => "DELETE",
        "deactivate" => "WRITE",
        "activate" => "WRITE",
        "editMenuPermission" => "WRITE",
        "generateMenu" => "READ",
        "paginatedList" => "READ",
        "addParentMenuPermission"=> "READ",
        "getMenuCode"=> "READ",
        "getMenuChildren"=> "READ",
        "getMenuPermission"=> "READ",
        "addParentMenuPermission"=> "READ",
        "getMenuPermissionSettings"=> "READ",
        "getMenuPermission"=> "READ",
        "editMenuEntitiesPermission"=> "READ",
        "getEntitiesPermissionSettings"=> "READ",
        "getEntitiesSettings"=> "READ"
    ]
];