<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::group(['prefix' => 'api/v2'], function () {
    Route::resource('login', 'AccountController');
    Route::get('logout', 'AccountController@logout');
});

Auth::routes();

Route::get('/app', 'HomeController@index');

Route::group(['prefix' => 'api/v2', 'namespace' => 'Configurations'], function () {

    Route::get('country/paginated-list', 'CountryController@paginatedList');
    Route::resource('country', 'CountryController');

    Route::get('university/paginated-list', 'UniversityController@paginatedList');
    Route::resource('university', 'UniversityController');

    Route::get('maritalstatus/paginated-list', 'MaritalstatusController@paginatedList');
    Route::resource('maritalstatus', 'MaritalstatusController');
    Route::get('relationship/paginated-list', 'RelationshipController@paginatedList');
    Route::resource('relationship', 'RelationshipController');

    Route::get('skill/paginated-list', 'SkillController@paginatedList');
    Route::resource('skill', 'SkillController');

    Route::get('education/paginated-list', 'EducationController@paginatedList');
    Route::resource('education', 'EducationController');

    Route::get('bloodGroup/paginated-list', 'BloodGroupController@paginatedList');
    Route::resource('bloodGroup', 'BloodGroupController');

    Route::get('post/paginated-list', 'PostController@paginatedList');
    Route::resource('post', 'PostController');

    Route::get('role/paginated-list', 'RoleController@paginatedList');
    Route::resource('role', 'RoleController');

    Route::get('religion/paginated-list', 'ReligionController@paginatedList');
    Route::resource('religion', 'ReligionController');

});

/***
 * Menu Setting
 */
//Menu settings

Route::group(['prefix' => 'api/v2', 'namespace' => 'Security'], function () {

    Route::resource('menu', 'Menu\MenuController');

    Route::post('menu/deactivate/{id}', 'Menu\MenuController@deactivate');
    Route::post('menu/activate/{id}', 'Menu\MenuController@activate');
    Route::get('generate-menu/{role_id}/{show_all}/{is_visible}', 'Menu\MenuController@generateMenu');
    Route::get('menu-code/{id_menu}', 'Menu\MenuController@getMenuCode');

    Route::resource('menu-permission', 'MenuPermission\MenuPermissionController');
    Route::post('menu-permission/deactivate', 'MenuPermission\MenuPermissionController@deactivate');
    Route::get('get-menu-children/{menu_id}', 'Menu\MenuController@getMenuChildren');
    Route::get('get-menu-permission/{role_id}', 'Menu\MenuController@getMenuPermission');
    Route::post('add-menu-permission/{parent_menu_id}','Menu\MenuController@addParentMenuPermission');
    Route::post('edit-menu-permission', 'Menu\MenuController@editMenuPermission');
    Route::get('get-menu-permission-settings/{menu_id}','Menu\MenuController@getMenuPermissionSettings');
    Route::post('menu-permission/configure', 'MenuPermission\MenuPermissionController@configure');
   // Route::post('entity-permission/configure', 'EntityPermission\EntityPermissionController@configure');
});
